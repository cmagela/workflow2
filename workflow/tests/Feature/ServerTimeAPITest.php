<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ServerTimeAPITest extends TestCase
{
    /**
     * Testing return format
     *
     * @return void
     */
    public function testRetornoServerTime() {
        $response = $this->get('/api/server-time');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'datetime'
        ]);
        $regex = '/[0-9]{2}[-|\/]{1}[0-9]{2}[-|\/]{1}[0-9]{4}/m';
        $this->assertEquals(preg_match_all($regex, $response['datetime']), 1);
    }
}